﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

class Edge
{
    public int vertex1, vertex2;

    public int weight;

    public Edge(int vertex1, int vertex2, int weight)
    {
        this.vertex1 = vertex1;
        this.vertex2 = vertex2;
        this.weight = weight;
    }
}

class Program
{
    /// <summary>
    /// Считывает из файла input.txt граф
    /// </summary>
    /// <returns>List объектов Edge</returns>
    static List<Edge> ReadGraphFromFile(string fileName)
    {
        List<Edge> E = new List<Edge>();
        try
        {
            StreamReader f = new StreamReader(fileName);
            int n = int.Parse(f.ReadLine());
            for (int i = 0; i < n; i++)
            {
                string[] temp = f.ReadLine().Split(' ');
                for (int j = 0; j < n; j++)
                {
                    if (i < j && temp[j] != "~")
                        E.Add(new Edge(i, j, int.Parse(temp[j])));
                }
            }
            f.Close();
        }
        catch
        {
            Console.WriteLine("Файл не найден");
        }
        return E;
    }

    /// <summary>
    /// Печать графа в файл
    /// </summary>
    /// <param name="fileName">Путь до файла output.txt</param>
    /// <param name="graph">Граф для печати</param>
    static void PrintInFile(string fileName, List<Edge> graph)
    {
        int sum = 0;
        foreach (Edge e in graph)
            sum += e.weight;
        try
        {
            StreamWriter fs = new StreamWriter(fileName);
            fs.WriteLine(sum);
            foreach(Edge e in graph)
                fs.WriteLine($"{e.vertex1} {e.vertex2} {e.weight}");
            fs.Close();
        }
        catch
        {
            Console.WriteLine("Файл не найден");
        }
    }

    /// <summary>
    /// Метод нахождения минимального остовного графа методом Прима
    /// </summary>
    /// <param name="graph">Граф</param>
    /// <returns>Остовное дерево</returns>
    static public List<Edge> AlgorithmPrim(List<Edge> graph)
    {
        List<Edge> ostovGraph = new List<Edge>();
        int numberVertex = graph.Count;
        List<Edge> notUsedEdge = new List<Edge>(graph);
        List<int> usedVertex = new List<int>();
        List<int> notUsedVertex = new List<int>();
        for (int i = 0; i < numberVertex; i++)
            notUsedVertex.Add(i);
        usedVertex.Add(0);
        notUsedVertex.RemoveAt(usedVertex[0]);
        while (notUsedVertex.Count > 0)
        {
            int minEdge = -1;
            for (int i = 0; i < notUsedEdge.Count; i++)
                if ((usedVertex.IndexOf(notUsedEdge[i].vertex1) != -1) && (notUsedVertex.IndexOf(notUsedEdge[i].vertex2) != -1) ||
                    (usedVertex.IndexOf(notUsedEdge[i].vertex2) != -1) && (notUsedVertex.IndexOf(notUsedEdge[i].vertex1) != -1))
                    if (minEdge != -1)
                    {
                        if (notUsedEdge[i].weight < notUsedEdge[minEdge].weight)
                            minEdge = i;
                    }
                    else
                        minEdge = i;
            try
            {
                if (usedVertex.IndexOf(notUsedEdge[minEdge].vertex1) != -1)
                {
                    usedVertex.Add(notUsedEdge[minEdge].vertex2);
                    notUsedVertex.Remove(notUsedEdge[minEdge].vertex2);
                }
                else
                {
                    usedVertex.Add(notUsedEdge[minEdge].vertex1);
                    notUsedVertex.Remove(notUsedEdge[minEdge].vertex1);
                }
            }
            catch
            {
                return ostovGraph;
            }
            ostovGraph.Add(notUsedEdge[minEdge]);
            notUsedEdge.RemoveAt(minEdge);
        }
        return ostovGraph;
    }

    static void Main(string[] args)
    {
        Stopwatch stopWatch = new Stopwatch();
        stopWatch.Start();
        List<Edge> Graph = ReadGraphFromFile("input.txt");
        if (Graph.Count > 1)
        {
            Graph = AlgorithmPrim(Graph);
            PrintInFile("output.txt", Graph);
        }
        else 
            PrintInFile("output.txt", Graph);
        stopWatch.Stop();
        TimeSpan ts = stopWatch.Elapsed;
        Console.WriteLine("Исходный файл... input.txt");
        Console.WriteLine($"Время работы: {ts.Milliseconds} млсек");
        Console.WriteLine("Результат работы программы в файле output.txt");
        Console.ReadKey();
    }
}