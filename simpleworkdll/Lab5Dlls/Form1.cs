﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Lab5Dlls {
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();
            graph.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
        }

        [DllImport("../../../Debug/MathDll.dll")]
        static extern IntPtr Quadratic(double a, double b, double c);

        [DllImport("../../../Debug/MathDll.dll")]
        static extern IntPtr SinWithCoefficient(double a, double b);

        [DllImport("../../../Debug/MathDll.dll")]
        static extern IntPtr Line(double a, double b);

        [DllImport(@"../../../Debug/MathDll.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern int ReleaseMemory(IntPtr ptr);

        private void button1_Click(object sender, EventArgs e) {
            IntPtr ptr = Quadratic(1, 0, -1);
            double[] mas = new double[100];
            double a = 0, b = 0, c = 0;
            try {
                if (listBox1.Text == listBox1.Items[0]) {
                    a = Double.Parse(textBox1.Text);
                    b = Double.Parse(textBox2.Text);
                    c = Double.Parse(textBox3.Text);
                    ptr = Quadratic(a, b, c);
                }
                if (listBox1.Text == listBox1.Items[1]) {
                    a = Double.Parse(textBox1.Text);
                    b = Double.Parse(textBox2.Text);
                    c = Double.Parse(textBox3.Text);
                    ptr = SinWithCoefficient(a, b);
                }
                if (listBox1.Text == listBox1.Items[2]) {
                    a = Double.Parse(textBox1.Text);
                    b = Double.Parse(textBox2.Text);
                    ptr = Line(a, b);
                }
                Marshal.Copy(ptr, mas, 0, 100);
                ReleaseMemory(ptr);
                graph.Series[0].Points.Clear();
                for (int i = 0; i < 100; i++)
                    graph.Series[0].Points.AddXY(-5 + i / 10.0, mas[i]);
            } catch {
                MessageBox.Show("Bad coefficients");
            }
        }
    }
}
