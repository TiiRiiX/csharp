// MathDll.cpp: ���������� ���������������� ������� ��� ���������� DLL.

#include "stdafx.h"
#include <math.h>

extern "C"
{
	__declspec(dllexport) double* Quadratic(double a, double b, double c)
	{
		double* arr = new double[100];
		for (int i = 0; i < 100; i++) {
			double step = -5 + i / 10.0;
			arr[i] = a*step*step + b*step * c;
		}
		return arr;
	}

	__declspec(dllexport) double* SinWithCoefficient(double a, double b) {
		double* arr = new double[100];
		for (int i = 0; i < 100; i++) {
			double step = -5 + i / 10.0;
			arr[i] = a*sin(step) + b;
		}
		return arr;
	}

	__declspec(dllexport) double* Line(double a, double b) {
		double* arr = new double[100];
		for (int i = 0; i < 100; i++) {
			double step = -5 + i / 10.0;
			arr[i] = a*step + b;
		}
		return arr;
	}


	__declspec(dllexport) int ReleaseMemory(int* pArray) {
		delete[] pArray;
		return 0;
	}
}

