﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleCalculator {
    class Program {
        static void Main(string[] args) {
            var twoArgumentSpliter = new char[] { '/', '*', '+', '-', '^' };
            Console.Write("Введите выражение:");
            var input = Console.ReadLine();
            var values = new List<Double>();
            if (input.IndexOfAny(twoArgumentSpliter) != -1)
                try {
                    foreach (var value in input.Split(twoArgumentSpliter))
                        values.Add(Double.Parse(value));
                    switch ((twoArgumentSpliter.Where(x => input.IndexOfAny(twoArgumentSpliter) != 1)).ToString()[0]) {
                        case '/': {
                            Console.WriteLine($"{values[0]} / {values[1]} = {values[0] / values[1]}");
                            break;
                        }
                        case '*': {
                            Console.WriteLine($"{values[0]} * {values[1]} = {values[0] * values[1]}");
                            break;
                        }
                        case '+': {
                            Console.WriteLine($"{values[0]} + {values[1]} = {values[0] + values[1]}");
                            break;
                        }
                        case '-': {
                            Console.WriteLine($"{values[0]} - {values[1]} = {values[0] - values[1]}");
                            break;
                        }
                        case '^': {
                            Console.WriteLine($"{values[0]} ^ {values[1]} = {Math.Pow(values[0], values[1])}");
                            break;
                        }

                    }
                } catch (FormatException) {
                    Console.WriteLine("Неверный ввод чисел");
                }
            else {
                Console.WriteLine("Действие");
            }
        }
    }
}
