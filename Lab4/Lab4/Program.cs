﻿using System;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;
using System.IO.MemoryMappedFiles;

namespace Lab4 {
    class FileReader {
        const uint GENERIC_READ = 0x80000000;
        const uint OPEN_EXISTING = 3;
        System.IntPtr handle;

        [System.Runtime.InteropServices.DllImport("kernel32", SetLastError = true)]
        static extern unsafe System.IntPtr CreateFile
        (
            string FileName,          // file name
            uint DesiredAccess,       // access mode
            uint ShareMode,           // share mode
            uint SecurityAttributes,  // Security Attributes
            uint CreationDisposition, // how to create
            uint FlagsAndAttributes,  // file attributes
            int hTemplateFile         // handle to template file
        );

        [System.Runtime.InteropServices.DllImport("kernel32", SetLastError = true)]
        static extern unsafe bool ReadFile
        (
            System.IntPtr hFile,      // handle to file
            void* pBuffer,            // data buffer
            int NumberOfBytesToRead,  // number of bytes to read
            int* pNumberOfBytesRead,  // number of bytes read
            int Overlapped            // overlapped buffer
        );

        [System.Runtime.InteropServices.DllImport("kernel32", SetLastError = true)]
        static extern unsafe bool CloseHandle
        (
            System.IntPtr hObject // handle to object
        );

        public bool Open(string FileName) {
            handle = CreateFile(FileName, GENERIC_READ, 0, 0, OPEN_EXISTING, 0, 0);

            if (handle != System.IntPtr.Zero)
                return true;
            else
                return false;
        }

        public unsafe int Read(byte[] buffer, int index, int count) {
            int n = 0;
            fixed (byte* p = buffer) {
                if (!ReadFile(handle, p + index, count, &n, 0))
                    return 0;
            }
            return n;
        }


        public bool Close() {
            return CloseHandle(handle);
        }

    }

    class Program {
        static string fileName;
        static int fileSize;
        static Stopwatch stopWatch = new Stopwatch();
        static byte[] buffer = new byte[1000000];

        static private void readWinApi() {
            FileReader fr = new FileReader();
            fr.Open(fileName);
            for (int i = 0; i < fileSize; i += buffer.Length)
                fr.Read(buffer, 0, buffer.Length);
            fr.Close();
        }

        static private void readFileStream() {
            FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            for (int i = 0; i < fileSize; i += buffer.Length)
                fs.Read(buffer, 0, buffer.Length);
            fs.Close();
        }

        static private void readMemoryMapping() {
            var mmf = MemoryMappedFile.CreateFromFile(fileName, FileMode.Open);
            var acc = mmf.CreateViewAccessor();
            byte b = new byte();
            for (int i = 0; i < fileSize; i += buffer.Length)
                acc.Read(i, out b);
            acc.Dispose();
            mmf.Dispose();
        }

        static private void readBufferedStream() {
            FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            BufferedStream bs = new BufferedStream(fs);
            int a;
            for (int i = 0; i < fileSize; i += buffer.Length)
                a = bs.ReadByte();
            bs.Close();
            fs.Close();
        }

        static void Main(string[] args) {
            Console.Write("Введите размер файла в мегабайтах: ");
            fileSize = Int32.Parse(Console.ReadLine()) * 1024 * 1024;
            fileName = @"file.txt";

            Console.Write($"Создание файла {fileName}...");
            var fs = File.Create(fileName);
            for (int i = 0; i < buffer.Length; i++)
                buffer[i] = 1;

            stopWatch.Start();

            for (int i = 0; i < fileSize; i += buffer.Length)
                fs.Write(buffer, 0, buffer.Length);

            stopWatch.Stop();
            fs.Close();

            var time = stopWatch.Elapsed;
            Console.WriteLine($" создан за {time.Seconds:00}.{time.Milliseconds:000} сек");

            Console.Write(
                "1 - Через файловые переменный\n" +
                "2 - Через WinApi\n" +
                "3 - Через файловые потоки\n" +
                "4 - Через отображение на память\n" +
                "Напишите необходимые действия через пробел:");
            var chooseList = new List<String>(Console.ReadLine().Split(' '));

            if (chooseList.Contains("1")) {
                Console.WriteLine("Чтение через FileStream...");
                stopWatch.Restart();

                readFileStream();

                stopWatch.Stop();
                time = stopWatch.Elapsed;
                Console.WriteLine($"Завершено за {time.Seconds:00}.{time.Milliseconds:00000} сек\n");
            }

            if (chooseList.Contains("2")) {
                Console.WriteLine("Чтение через WinApi...");
                stopWatch.Restart();

                readWinApi();

                stopWatch.Stop();
                time = stopWatch.Elapsed;
                Console.WriteLine($"Завершено за {time.Seconds:00}.{time.Milliseconds:00000} сек\n");
            }

            if (chooseList.Contains("3")) {
                Console.WriteLine("Чтение через BufferedStream...");
                stopWatch.Restart();

                readBufferedStream();

                stopWatch.Stop();
                time = stopWatch.Elapsed;
                Console.WriteLine($"Завершено за {time.Seconds:00}.{time.Milliseconds:00000} сек\n");
            }

            if (chooseList.Contains("4")) {
                Console.WriteLine("Чтение через MemoryMapping...");
                stopWatch.Restart();

                readMemoryMapping();

                stopWatch.Stop();
                time = stopWatch.Elapsed;
                Console.WriteLine($"Завершено за {time.Seconds:00}.{time.Milliseconds:00000} сек\n");
            }
            File.Delete(fileName);
            Console.ReadKey();
        }
    }
}
