﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class Form1 : Form
    {
        double value1 = 0, value2 = 0, value_res = 0;
        char select = '0';

        public Form1()
        {
            InitializeComponent();
        }

        #region button_ARIF#_Click
        private void button_plus_Click(object sender, EventArgs e)
        {
            button_ARIF_Click('+');
        }
        private void button_minus_Click(object sender, EventArgs e)
        {
            button_ARIF_Click('-');
        }
        private void button_mult_Click(object sender, EventArgs e)
        {
            button_ARIF_Click('x');
        }
        private void button_dev_Click(object sender, EventArgs e)
        {
            button_ARIF_Click('/');
        }
        public void button_ARIF_Click(char b_select)
        {
            if (screen_select.Text != "")
            {
                if (screen.Text != "")
                    button_equal.PerformClick();
            } else if (screen.Text != "" || screen_memory.Text == "")
            {
                try
                {
                    screen_memory.Text = Convert.ToString(Convert.ToDouble(screen.Text));
                } catch (FormatException) { /*throw;*/ screen_memory.Text = "Ошибка!"; } finally
                {
                    screen.Clear();
                }
            }
            screen_select.Text = Convert.ToString(b_select);
            /*}*/
            /*catch (FormatException) { }
            finally
            {*/
            screen.Focus();
            /*}*/
        }
        #endregion
        private void button_equal_Click(object sender, EventArgs e)
        {
            string s = "";
            try
            {
                if (screen.Text == "")
                    throw new NoArgumentsException();
                if (screen_select.Text == "")
                    throw new NoSelectionsException();
                value1 = Convert.ToDouble(screen_memory.Text);
                value2 = Convert.ToDouble(screen.Text);
                select = Convert.ToChar(screen_select.Text);
                switch (select)
                {
                    case '+':
                    value_res = value1 + value2;
                    break;
                    case '-':
                    value_res = value1 - value2;
                    break;
                    case 'x':
                    value_res = value1 * value2;
                    break;
                    case '/':
                    if (value2 == 0)
                        throw new DividedZerroException();
                    else
                        value_res = value1 / value2;
                    break;
                    default:
                    value_res = 0;
                    break;
                }
                s = Convert.ToString(value_res);
                if (s.Length > screen_memory.MaxLength)
                {
                    if (Convert.ToString(Math.Truncate(value_res)).Length < screen.MaxLength)
                    {
                        s = s.Substring(0, screen.MaxLength);
                    } else
                        throw new OverflowException();
                }
            } catch (DividedZerroException)
            {
                s = "Дел. на 0";
            } catch (FormatException)
            {
                s = "Ошибка!";
            } catch (OverflowException)
            {
                s = "Многоцифр";
            } catch (NoArgumentsException)
            {
                s = "";
            } catch (NoSelectionsException)
            {
                s = screen.Text;
            } finally
            {
                screen_memory.Text = s;
                value1 = value2 = value_res = 0;
                select = '0';
                screen.Focus();
                screen.Clear();
                screen_select.Clear();
            }
        }
        #region button#_Click
        private void button0_Click(object sender, EventArgs e)
        {
            if (screen.TextLength < screen.MaxLength)
                screen.AppendText("0");
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (screen.TextLength < screen.MaxLength)
                screen.AppendText("1");
        }
        private void button2_Click(object sender, EventArgs e)
        {
            if (screen.TextLength < screen.MaxLength)
                screen.AppendText("2");
        }
        private void button3_Click(object sender, EventArgs e)
        {
            if (screen.TextLength < screen.MaxLength)
                screen.AppendText("3");
        }
        private void button4_Click(object sender, EventArgs e)
        {
            if (screen.TextLength < screen.MaxLength)
                screen.AppendText("4");
        }
        private void button5_Click(object sender, EventArgs e)
        {
            if (screen.TextLength < screen.MaxLength)
                screen.AppendText("5");
        }
        private void button6_Click(object sender, EventArgs e)
        {
            if (screen.TextLength < screen.MaxLength)
                screen.AppendText("6");
        }
        private void button7_Click(object sender, EventArgs e)
        {
            if (screen.TextLength < screen.MaxLength)
                screen.AppendText("7");
        }
        private void button8_Click(object sender, EventArgs e)
        {
            if (screen.TextLength < screen.MaxLength)
                screen.AppendText("8");
        }
        private void button9_Click(object sender, EventArgs e)
        {
            if (screen.TextLength < screen.MaxLength)
                screen.Text = "9" + screen.Text;
        }
        #endregion

        private void button_dot_Click(object sender, EventArgs e)
        {
            if (screen.TextLength < screen.MaxLength - 1)
            {
                try
                {
                    Convert.ToDouble(screen.Text + ",");
                    screen.Text = "," + screen.Text;
                } catch (FormatException)
                { }
            }

        }

        private void button_clear_all_Click(object sender, EventArgs e)
        {
            screen_memory.Clear();
            screen_select.Clear();
            screen.Clear();
        }

        private void screen_KeyDown(object sender, KeyEventArgs e)
        {
            int a = e.KeyValue;
            if (!(a >= 96 && a <= 105) && !((a >= 48 && a <= 57) && !e.Shift))
            {
                e.SuppressKeyPress = true;
                if (a == 13)//enter
                    button_equal.PerformClick();

                else if (a == 107)//+
                    button_plus.PerformClick();

                else if (a == 109 || a == 189)//-
                    button_minus.PerformClick();

                else if (a == 88 || a == 106 || a == 219)//x
                    button_mult.PerformClick();

                else if (a == 111 || a == 220)// деление
                    button_div.PerformClick();
                else if (a == 188)//,
                    button_dot.PerformClick();




                else if (a == 8)//backspace
                {
                    if (screen.Text.Length != 0)
                        screen.Text = screen.Text.Substring(0, screen.Text.Length - 1);
                }
            }
        }
        #region TRIGONOM
        private void button_sin_Click(object sender, EventArgs e)
        {
            TRIGONOM("sin");
        }

        private void button_cos_Click(object sender, EventArgs e)
        {
            TRIGONOM("cos");
        }

        private void button_tg_Click(object sender, EventArgs e)
        {
            TRIGONOM("tg");
        }

        private void button_ctg_Click(object sender, EventArgs e)
        {
            TRIGONOM("ctg");
        }

        public void TRIGONOM(string select)
        {
            double value = 0;
            bool s = false;
            if (screen.Text != "")
            {
                value = Convert.ToDouble(screen.Text);
                s = true;
            } else if (screen_memory.Text != "")
                value = Convert.ToDouble(screen_memory.Text);
            else
                select = "";
            switch (select)
            {
                case "sin":
                value = Math.Sin(value);
                break;
                case "cos":
                value = Math.Cos(value);
                break;
                case "tg":
                value = Math.Tan(value);
                break;
                case "ctg":
                value = 1 / Math.Tan(value);
                break;
            }
            string res = Convert.ToString(value);

            if (res.Length > screen_memory.MaxLength)
                if (Convert.ToString(Math.Truncate(value)).Length < screen.MaxLength)
                {
                    res = res.Substring(0, screen.MaxLength);
                } else
                    res = "Многоцифр";
            if (s)
                screen.Text = res;
            else
            {
                screen_memory.Text = res;
                screen_select.Clear();
            }
        }
        #endregion
    }
    class DividedZerroException : ApplicationException
    {
        // Реализуем стандартные конструкторы,
        public DividedZerroException() : base() { }
        public DividedZerroException(string str) : base(str) { }
        // Переопределяем метод ToString() для класса
        public override string ToString()
        {
            return /*Message*/"Деление на ноль!";
        }
    }
    class OverflowException : ApplicationException
    {
        // Реализуем стандартные конструкторы,
        public OverflowException() : base() { }
        public OverflowException(string str) : base(str) { }
        // Переопределяем метод ToString() для класса
        public override string ToString()
        {
            return /*Message*/"Переполнение поля для ввода!";
        }
    }
    class NoArgumentsException : ApplicationException
    {
        // Реализуем стандартные конструкторы,
        public NoArgumentsException() : base() { }
        public NoArgumentsException(string str) : base(str) { }
        // Переопределяем метод ToString() для класса
        public override string ToString()
        {
            return /*Message*/"Не введено аргументов!";
        }
    }
    class NoSelectionsException : ApplicationException
    {
        // Реализуем стандартные конструкторы,
        public NoSelectionsException() : base() { }
        public NoSelectionsException(string str) : base(str) { }
        // Переопределяем метод ToString() для класса
        public override string ToString()
        {
            return /*Message*/"Выбор операции не сделан!";
        }
    }
}
