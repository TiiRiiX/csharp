using System;
using System.Linq;

namespace ConsoleApp1 {
    class Program {
        public static int[] FindArrayWithMaxSum(int[] array, int m) {
            var index = 0;
            var localSum = 0;
            for (int i = 0; i < m; i++)
                localSum += array[i];

            var globalSum = localSum;

            for (int i = m; i < array.Length; i++) {
                localSum += array[i] - array[i - m];
                if (localSum > globalSum) {
                    globalSum = localSum;
                    index = i - m + 1;
                }
            }
            Console.WriteLine($"Штука {globalSum} и штука {index}");
            return array.ToList().GetRange(index, m).ToArray();
        }

        static void Main(string[] args) {
            var array = FindArrayWithMaxSum(new[] { 9, 9, 1, 2, 9 }, 2);
            Console.WriteLine($"{String.Join(" ", array)} Сумма:{array.Sum()}");
        }
    }
}
