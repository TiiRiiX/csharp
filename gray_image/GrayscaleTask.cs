﻿using System.Diagnostics;

namespace Recognizer
{
	public static class GrayscaleTask
	{
        public static double[,] Gray1(Pixel[,] original)
        {
            var time = new Stopwatch();
            var res = new double[original.GetLength(0), original.GetLength(1)];
            for (var i = 0; i < original.GetLength(0); i++)
                for (var j = 0; j < original.GetLength(1); j++)
                    res[i, j] = (0.299 * original[i, j].R + 0.582 * original[i, j].G + 0.114 * original[i, j].B) / 255;
            return res;
        }

        public static double[,] Gray2(Pixel[,] original)
        {
            var time = new Stopwatch();
            var n = original.GetLength(0);
            var m = original.GetLength(1);
            var res = new double[n, m];
            for (var i = 0; i < n; i++)
                for (var j = 0; j < m; j++)
                    res[i, j] = (0.299 * original[i, j].R + 0.582 * original[i, j].G + 0.114 * original[i, j].B) / 255;
            return res;
        }

        public static double[,] Gray3(Pixel[,] original)
        {
            var time = new Stopwatch();
            var n = original.GetLength(0);
            var m = original.GetLength(1);
            Pixel p;
            var res = new double[n, m];
            for (var i = 0; i < n; i++)
                for (var j = 0; j < m; j++)
                {
                    p = original[i, j];
                    res[i, j] = (0.299 * p.R + 0.582 * p.G + 0.114 * p.B) / 255;
                }
            return res;
        }

        unsafe public static double[,] Gray4(Pixel[,] original)
        {
            var time = new Stopwatch();
            var n = original.GetLength(0);
            var m = original.GetLength(1);
            var res = new double[n, m];
            Pixel p;
            fixed (double* ptr = &res[0, 0])
            {
                double* curPtr = ptr;
                for (var i = 0; i < n; i++)
                    for (var j = 0; j < m; j++, curPtr++)
                    {
                        p = original[i, j];
                        *(curPtr) = (0.299 * p.R + 0.582 * p.G + 0.114 * p.B) / 255;
                    }
            }
            return res;
        }


        public static double[,] ToGrayscale(Pixel[,] original)
		{
            var time = new Stopwatch();
            time.Restart();
            Gray4(original);
            Debug.WriteLine($"Time: {time.ElapsedTicks}");
            time.Stop();
            return Gray4(original);
		}
	}
}