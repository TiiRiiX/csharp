﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ApBots.Utils;

namespace ApBots.MessagesHandler.Modules.Common {
    public class Ascii : IModule {
        public string[] Aliases { get; } = new string[] {
            "^аски"
        };

        public Regex Expression { get; }

    #region alphabet

        public static Dictionary<char, string[]> alphabet = new Dictionary<char, string[]>()
        {
            ['A'] = new string[]{
                    "  #  ",
                    " # # ",
                    "#   #",
                    "#####",
                    "#   #"
                },
            ['B'] = new string[]{
                    "#### ",
                    "#   #",
                    "#### ",
                    "#   #",
                    "#### "
                },
            ['C'] = new string[]{
                    " ####",
                    "#    ",
                    "#    ",
                    "#    ",
                    " ####"
                },
            ['D'] = new string[]{
                    "#### ",
                    "#   #",
                    "#   #",
                    "#   #",
                    "#### "
                },
            ['E'] = new string[]{
                    "#####",
                    "#    ",
                    "###  ",
                    "#    ",
                    "#####"
                },
            ['F'] = new string[]{
                    "#####",
                    "#    ",
                    "###  ",
                    "#    ",
                    "#    "
                },
            ['G'] = new string[]{
                    " ####",
                    "#    ",
                    "#  ##",
                    "#   #",
                    " ####"
                },
            ['H'] = new string[]{
                    "#   #",
                    "#   #",
                    "#####",
                    "#   #",
                    "#   #"
                },
            ['I'] = new string[]{
                    "###",
                    " # ",
                    " # ",
                    " # ",
                    "###"
                },
            ['J'] = new string[]{
                    "  ###",
                    "    #",
                    "    #",
                    "#   #",
                    " ### "
                },
            ['K'] = new string[]{
                    "#   #",
                    "# ## ",
                    "##   ",
                    "# ## ",
                    "#   #"
                },
            ['L'] = new string[]{
                    "#    ",
                    "#    ",
                    "#    ",
                    "#    ",
                    "#####"
                },
            ['M'] = new string[]{
                    "#   #",
                    "## ##",
                    "# # #",
                    "#   #",
                    "#   #"
                },
            ['N'] = new string[]{
                    "#   #",
                    "##  #",
                    "# # #",
                    "#  ##",
                    "#   #"
                },
            ['O'] = new string[]{
                    " ### ",
                    "#   #",
                    "#   #",
                    "#   #",
                    " ### "
                },
            ['P'] = new string[]{
                    "#### ",
                    "#   #",
                    "#### ",
                    "#    ",
                    "#    "
                },
            ['Q'] = new string[]{
                    " ##  ",
                    "#  # ",
                    "#  # ",
                    " ##  ",
                    "   ##"
                },
            ['R'] = new string[]{
                    "#### ",
                    "#   #",
                    "#### ",
                    "# #  ",
                    "#  # "
                },
            ['S'] = new string[]{
                    " ### ",
                    "#    ",
                    " ### ",
                    "    #",
                    " ### "
                },
            ['T'] = new string[]{
                    "#####",
                    "  #  ",
                    "  #  ",
                    "  #  ",
                    "  #  "
                },
            ['U'] = new string[]{
                    "#   #",
                    "#   #",
                    "#   #",
                    "#   #",
                    " ### "
                },
            ['V'] = new string[]{
                    "#   #",
                    "#   #",
                    " # # ",
                    " # # ",
                    "  #  "
                },
            ['W'] = new string[]{
                    "#   #   #",
                    "#   #   #",
                    " # # # # ",
                    " # # # # ",
                    "  #   #  "
                },
            ['X'] = new string[]{
                    "#   #",
                    " # # ",
                    "  #  ",
                    " # # ",
                    "#   #"
                },
            ['Y'] = new string[]{
                    "#   #",
                    " # # ",
                    "  #  ",
                    "  #  ",
                    "  #  "
                },
            ['Z'] = new string[]{
                    "#####",
                    "   # ",
                    "  #  ",
                    " #   ",
                    "#####"
                },
        };

    #endregion

        public string Execute(string text) {
            string answer = "";
            /*char[] ALPHABET = {
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я'
            };*/
            int H = 5;
            string[] mesage = text.ToUpper().Replace("АСКИ ","").Split(' ');
            //int[] message_num = new int[message.Length];
            for (int w = 0; w < mesage.Length; w++)
            {
                for (int i = 0; i < H; i++)
                {
                    for (int j = 0; j < mesage.Length; j++)
                    {
                        for (int k = 0; k < alphabet[mesage[w][j]][i].Length; k++)
                            if (alphabet[mesage[w][j]][i][k] == '#')
                                answer += "█";
                            else
                                answer += "─";
                        if (j != mesage.Length - 1)
                            answer += "─";
                    }
                    answer += "\n";
                }
                answer += "\n";
            }
            return answer;
        }

        public Ascii() {
            Expression = Aliases.GetRegex();
        }
    }
}
