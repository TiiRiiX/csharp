using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Solution
{
    static void Main(string[] args)
    {
        char[] ALPHABET = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','?'};
        int L = int.Parse(Console.ReadLine());
        int H = int.Parse(Console.ReadLine());
        string T = Console.ReadLine();
        T = T.ToUpper();
        Console.Error.WriteLine(T);
        int[] T_num = new int[T.Length];
        for (int i = 0; i < T.Length; i++){
            int t = Array.BinarySearch(ALPHABET, T[i]);
            Console.Error.Write(t + " ");
            if (t >= 0)
                T_num[i] = t;
            else
                T_num[i] = 26;
        }
        Console.Error.WriteLine(T_num[0]);
        char[,,] alphabet = new char[27,L,H];
        for (int i = 0; i < H; i++)
        {
            string ROW = Console.ReadLine();
            for (int j = 0; j < ROW.Length; j++)
                alphabet[j / L, j % L, i] = ROW[j];
        }
        for (int i = 0; i < H; i++){
            for (int j = 0; j < T_num.Length; j++)
                    for (int k = 0; k < L; k++)
                        Console.Write(alphabet[T_num[j],k,i]);
            Console.WriteLine();
        }
        // Write an action using Console.WriteLine()
        // To debug: Console.Error.WriteLine("Debug messages...");
    }
}