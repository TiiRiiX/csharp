using System;

class Org
{
    public int index;
    string city;
    string street;
    public int number;
    public Org(int index, string city, string street, int number)
    {
        this.index = index;
        this.city = city;
        this.street = street;
        this.number = number;
    }
    public void Show()
    {
        Console.WriteLine("Индекс организации: {0}\nГород: {1}\nУлица: {2}\nНомер дома: {3}\n", index, city, street, number);
    }
}

class Programm
{
    static void Main()
    {
        Org[] companies = new Org[5];
        companies[0] = new Org(614090, "Perm", "Ленина", 4);
        companies[1] = new Org(614000, "Perm", "Чкалова", 13);
        companies[2] = new Org(613000, "City", "Улица", 410);
        companies[3] = new Org(511233, "Еще какой-то город", "Еще какая-то улица", 666);
        companies[4] = new Org(666666, "Hell", "Трем", 512);
        int everegeNumber = 0;
        for (int i = 0; i < 5; i++)
            everegeNumber += companies[i].number;
        everegeNumber /= 5;
        for (int i = 0; i < 5; i++)
            if (companies[i].index <= 614000 && companies[i].number > everegeNumber)
                companies[i].Show();
    }
}