using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class oldCard //карты старого типа
{
    protected string fio; //поле фамилия имя отчество
    protected string birthday; //дата рождения

    public string Fio //свойство даты рождения
    {
        get { return fio; }
    }

    public oldCard(string fio, string birthday) //конструктор 
    {
        this.fio = fio;
        this.birthday = birthday;
    }

    public virtual void info() //вывод информации о клиенте
    {
        Console.WriteLine("Клиент: {0}\nДата рождения: {1}", fio, birthday);
    }
}

class newCard : oldCard //новые карты
{
    private string phone; //номер телефона

    public string Phone //свойство номер телефона
    {
        get { return phone; }
    }

    public newCard(string fio, string birthday, string phone):base(fio, birthday) //конструктор ссылающийся на базовый класс
    {
        this.fio = fio;
        this.birthday = birthday;
        this.phone = phone;
    }

    public override void info() //вывод информации
    {
        base.info(); //обращение к базовому классу
        Console.WriteLine("Номер телефона: {0}", phone);
    }
}


class Program
{
    static void Main(string[] args)
    {
        List<oldCard> shoppers = new List<oldCard>(); //коллекция карт
        try
        {
            StreamReader f = new StreamReader("input.txt"); //открываем поток на чтение
            while (!f.EndOfStream) //пока не достигнут конец файла
            {
                string[] temp = f.ReadLine().Split(' '); //читаем строку из файла и создаем из нее массив методом split
                if (temp.Length == 5) //если пять элеметнов, то это запись нового типа
                    shoppers.Add(new newCard(temp[0] + " " + temp[1] + " " + temp[2], temp[3], temp[4]));
                else //иначе старый тип
                    shoppers.Add(new oldCard(temp[0] + " " + temp[1] + " " + temp[2], temp[3]));
            }
            f.Close();//закрываем файл
        }
        catch (Exception e) //перехватываем ошибку
        {
            Console.WriteLine(e.Message); //выводим сообщение об ошибке
            Console.WriteLine("Входной файл не найден");
        }
        Console.WriteLine("Все достунпые записи: ");
        foreach (oldCard card in shoppers) //перебор всех элементов в списке с картами
        {
            card.info(); //выводим информацию о картах
            Console.WriteLine();
        }
        Console.WriteLine("-------------------------------------");
        Console.WriteLine("Поиск номера телефона по ФИО покупателя: Абрамова Анастасия Леонидовна");
        foreach (oldCard card in shoppers)
            if (card.Fio == "Абрамова Анастасия Леонидовна") //ищем карту с нужным нам именем
            {
                Console.WriteLine("Найден: {0}",((newCard)card).Phone); //выводим номер телефона
            }
        Console.WriteLine("-------------------------------------");
        Console.WriteLine("Вывод всех записей нового типа...");
        foreach (oldCard card in shoppers)
            if (card is newCard) //если запись принадлежит новому типу
            {
                card.info(); //выводим информацию
                Console.WriteLine();
            }
        Console.WriteLine("-------------------------------------");
        Console.WriteLine("Определения количества записей старого и нового типа...");
        int n1 = 0, n2 = 0; //счетчики количества записей
        foreach (oldCard card in shoppers)
            if (card is newCard)
                n2++; //увеличиваем счетчик новых записей
            else
                n1++; //увеличиваем счетчик старых записей
        Console.WriteLine("Количество записей старого типа: {0}\nКоличество записей нового типа: {1}", n1, n2); //выводим количество
    }
}

/* Содержимое файла input.txt
Баранова Регина Викторовна 24.05.1982 89036833688
Яковлев Галактион Федорович 5.09.1982 89035591467
Соколова Владилена Филипповна 5.12.1984 89241442978
Куколевская Эльвира Владиславовна 13.08.1967
Трофимов Сергей Денисович 21.08.1989
Абрамова Анастасия Леонидовна 7.04.1971 89073568319
Фомова Кира Степановна 1.06.1977
*/