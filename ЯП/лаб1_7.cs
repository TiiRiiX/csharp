using System;

class Programm
{
    static void Main()
    {
        double a, b, c, d, x1, x2;
        string str;
        Console.Write("Введите a: ");
        a = double.Parse(Console.ReadLine());
        Console.Write("Введите b: ");
        b = double.Parse(Console.ReadLine());
        Console.Write("Введите c: ");
        c = int.Parse(Console.ReadLine());
        d = System.Math.Pow(b, 2) - 4 * a * c;
        if (d < 0) { str = "Действительных корней нет!"; }
        else
        {
            x1 = (-b - System.Math.Sqrt(d)) / (2 * a);
            x2 = (-b + System.Math.Sqrt(d)) / (2 * a);
            str = "x1 = " + x1 + "\nx2 = " + x2;
        }
        Console.WriteLine(str);
        Console.WriteLine("Нажмите любую кнопку!");
        Console.ReadKey();
    }
}