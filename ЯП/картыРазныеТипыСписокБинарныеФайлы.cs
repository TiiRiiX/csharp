using System;
using System.IO;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

[Serializable]
class oldCard //карты старого типа
{
    protected string fio; //поле фамилия имя отчество
    protected int b_day; //день рождения
    protected int b_month; //месяц рождения
    protected int b_year;//год рождения

    public string Fio //свойство ФИО
    {
        get { return fio; }
    }

    public int B_month //свойство месяца рождения
    {
        get { return b_month; }
    }

    public oldCard(string fio, string birthday) //конструктор 
    {
        this.fio = fio;
        this.b_day = int.Parse(birthday.Split('.')[0]);
        this.b_month = int.Parse(birthday.Split('.')[1]);
        this.b_year = int.Parse(birthday.Split('.')[2]);
    }

    public virtual void info() //вывод информации о клиенте
    {
        Console.WriteLine("Клиент: {0}\nДата рождения: {1}.{2}.{3}", fio, b_day, b_month, b_year);
    }
}
[Serializable]
class newCard : oldCard //новые карты
{
    private string phone; //номер телефона

    public string Phone //свойство номер телефона
    {
        get { return phone; }
    }

    public newCard(string fio, string birthday, string phone) : base(fio, birthday) //конструктор ссылающийся на базовый класс
    {
        this.fio = fio;
        this.b_day = int.Parse(birthday.Split('.')[0]);
        this.b_month = int.Parse(birthday.Split('.')[1]);
        this.b_year = int.Parse(birthday.Split('.')[2]);
        this.phone = phone;
    }

    public override void info() //вывод информации
    {
        base.info(); //обращение к базовому классу
        Console.WriteLine("Номер телефона: {0}", phone);
    }
}
class Program
{
    static void Main(string[] args)
    {
        BinaryFormatter Format = new BinaryFormatter(); //создание объекта для десериализации
        FileStream fout = new FileStream("C:/txts/out.bin", FileMode.Open, FileAccess.Read); //открытие бинарного файла на чтение
        List<oldCard> shoppers = (List<oldCard>)Format.Deserialize(fout);//считывание списка
        fout.Close();//закрытие файла
        int smscout = 0;//счетчик отправленных смс
        int happy = 0;//количество человек с днем рождения
        foreach (oldCard card in shoppers) //перебор всех элементов списка
        {
            if (card.B_month == 3) //если номер месяца совпадает с текущим (3)
            { happy++; //увеличиваем счетчик всех людей с днем рождения
                if (card is newCard) //если есть номер телефона
                    smscout++; //отправляем сообщение
            }
        }
        Console.WriteLine("Количество покупателей с допольнительной скидкой: {0}", happy);//вывод в консоль
        Console.WriteLine("Количество отправленных сообщений: {0}", smscout);
    }
}