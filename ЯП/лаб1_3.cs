using System;

class Programm
{
    static void Main()
    {
        double eps = 0.0001;
        double xFirst = 1;
        Console.WriteLine(xFirst);
        double xSecond = System.Math.E/100;
        Console.WriteLine(xSecond);
        double sum = xFirst + xSecond;
        double znam = 100;
        while(xSecond > eps)
        {
            xFirst = xSecond;
            znam = 100 * 100;
            xSecond *= System.Math.E / (znam * znam);
            sum += xSecond;
            Console.WriteLine("{0:0.####}", xSecond);
        }
        Console.WriteLine("Sum = {0:0.####}", sum);
    }
}