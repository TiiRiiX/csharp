using System;

class Programm
{
    static void Main()
    {
        int Mathematics, Physics, Biology;
        Console.Write("Введите оценку за математику: ");
        Mathematics = int.Parse(Console.ReadLine());
        Console.Write("Введите оценку за физику: ");
        Physics = int.Parse(Console.ReadLine());
        Console.Write("Введите оценку за биологию: ");
        Biology = int.Parse(Console.ReadLine());
        string str_1, str_2;
        str_1 = "Конъюнкция: ";
        str_2 = "Дизъюнкция: ";
        if (Mathematics == 5 && Physics == 5 && Biology == 5) { str_1 += "Отличник"; } else { str_1 += "Двоечник"; }
        if (Mathematics == 5 || Physics == 5 || Biology == 5) { str_2 += "Молодец"; } else { str_2 += "Лентяй"; }
        Console.WriteLine(str_1);
        Console.WriteLine(str_2);
        Console.WriteLine("Нажмите любую кнопку!");
        Console.ReadKey();
    }
}