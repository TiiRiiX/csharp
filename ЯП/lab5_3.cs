using System;
using System.IO;
using System.Collections.Generic;

class Buyer
{
    private string surname;
    private string number;
    private string date;
    private string label;
    private string color;
    private int cost;

    public string Label
    {
        get { return label; }
    }
    public int Cost
    {
        get { return cost; }
    }
    public string Surname
    {
        get { return surname; }
    }
    public string Number
    {
        get { return number; }
    }

    public string Info
    {
        get { return $"Покупатель: {surname}\nНомер ВУ: {number}\nДата покупки:{date}\nМарка: {label}\nЦвет: {color}\nСтоимость: {cost}"; }
    }

    public Buyer(string surname, string number, string date, string label, string color, int cost)
    {
        this.surname = surname;
        this.number = number;
        this.date = date;
        this.label = label;
        this.color = color;
        this.cost = cost;
    }
}

class Program
{
    static void printAll(List<Buyer> printList)
    {
        foreach (Buyer b in printList)
        {
            Console.WriteLine(b.Info);
            Console.WriteLine();
        }
    }

    static float everageCost(List<Buyer> contList)
    {
        float sum = 0;
        foreach (Buyer b in contList)
            sum += b.Cost;
        return sum / contList.Count;
    }

    static void Main(string[] args)
    {
        StreamReader fin = new StreamReader(@"C:\txts\input2.txt");
        List<Buyer> listBuy = new List<Buyer>();
        while (!fin.EndOfStream)
        {
            string[] temp = fin.ReadLine().Split(' ');
            listBuy.Add(new Buyer(temp[0], temp[1], temp[2], temp[3], temp[4], int.Parse(temp[5])));
        }
        fin.Close();
        //printAll(listBuy);
        //Console.WriteLine($"Средняя цена: {everageCost(listBuy)}");
        //Console.WriteLine();
        List<Buyer> listToSort = new List<Buyer>();
        foreach(Buyer b in listBuy)
        {
            foreach (Buyer b2 in listBuy)
                if (b.Label == b2.Label && b2 != b)
                {
                    listToSort.Add(b);
                }
        }
        for(int i = 0; i<listToSort.Count + 1; i++) {
            int maxCost = 0;
            Buyer tempBuy = null;
            foreach (Buyer b2 in listToSort)
                if (b2.Cost > maxCost)
                {
                    maxCost = b2.Cost;
                    tempBuy = b2;
                }
            Console.WriteLine(tempBuy.Info);
            Console.WriteLine();
            listToSort.Remove(tempBuy);
        }
        StreamWriter fout = new StreamWriter("C:/txts/out.txt");
        fout.WriteLine($"Средняя сумма покупок: {everageCost(listBuy)}");
        int mincost = Int32.MaxValue;
        Buyer minBuy = null;
        int maxcost = 0;
        Buyer maxBuy = null;
        foreach (Buyer b in listBuy)
        {
            if (b.Cost < mincost)
            {
                mincost = b.Cost;
                minBuy = b;
            }
            if (b.Cost > maxcost)
            {
                maxcost = b.Cost;
                maxBuy = b;
            }
        }
        fout.WriteLine($"Максимальная покупка: {maxBuy.Surname} {maxBuy.Number} сумма покупки = {maxBuy.Cost}");
        fout.WriteLine($"Минимальная покупка: {minBuy.Surname} {minBuy.Number} сумма покупки = {minBuy.Cost}");
        fout.Close();
    }
}

/*
Ершов 351710058163709 21.11.2003 RenaultSymbol Белый 722
Алексеева 351549000271610 31.01.2013 RenaultClio Античный белый 34
Кузьмина 355114059407684 22.03.2013 KoenigseggCCX Спаржа 6
Красильников 358240051855963 09.10.2035 VolkswagenType2 Красный 768
Ефимова 011245007057266 11.03.2022 Porsche 911GT3Cup Спаржа 334
Казакова 359254069124352 06.09.2002 ToyotaCorolla Зеленый 166
Зыкова 358815051663903 20.08.2003 Mercedes-BenzC-Class Бистр 916
Миронова 358755054792988 11.08.2022 StudebakerChampion Фиолетовый 444
Орехова 353166059069825 31.10.2021 LamborghiniGallardo Белый 153
Мельникова 013327002019901 18.03.2001 KoenigseggCCX Аквамариновый 196
*/
