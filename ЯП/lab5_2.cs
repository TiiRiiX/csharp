﻿using System;
using System.IO;
using System.Collections.Generic;

class Faculty
{
    private string name;
    private int year;
    private int countStudent;
    private int price;

    public int Year
    {
        get { return year; }
    }

    public int Price
    {
        get { return price; }
    }

    public string info
    {
        get { return $"Факультет {name} основан в {year} насчитывает {countStudent} студентов\nСтоимость обучения: {price}"; }
    }

    public Faculty(string name, int year, int countStudent, int price)
    {
        this.name = name;
        this.year = year;
        this.countStudent = countStudent;
        this.price = price;
    }
}


class Program
{
    static void Main(string[] args)
    {
        List<Faculty> facults = new List<Faculty>();
        StreamReader fs = new StreamReader("C:/txts/input.txt");
        while (!fs.EndOfStream)
        {
            string[] temp = fs.ReadLine().Split(' ');
            facults.Add(new Faculty(temp[0], int.Parse(temp[1]), int.Parse(temp[2]), int.Parse(temp[3])));
        }
        fs.Close();
        int everageSum = 0;
        foreach (Faculty f in facults)
        {
            everageSum += f.Price;
        }
        everageSum /= facults.Count;
        StreamWriter fout = new StreamWriter("C:/txts/out.txt");
        foreach (Faculty f in facults)
            if (f.Year <= 2000 && f.Price <= everageSum)
                fout.WriteLine(f.info);
        fout.Close();
    }
}
/*
Юридический 2012 35126 235662
Географический 1999 1251 162373
Геологический 1998 2355 4262763
Биологический 2001 2624 246261
Физический 2003 3415 131623 
 */
