using System;

class Person
{
    protected string name;
    protected string surname;
    protected int age;
    public string sex;
    public Person(string name, string surname, int age, string sex)
    {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.sex = sex;
    }
    public void Show()
    {
        Console.WriteLine("Имя: {0}\nФамилия: {1}\nВозраст: {2}\nПол: {3}", name, surname, age, sex);
    }
}

class Employee : Person
{
    public string Company;
    string Position;
    int Salary;
    public Employee(string Company, string Position, int Salary, string name, string surname, int age, string sex):base(name, surname,age ,sex)
    {
        this.sex = sex;
        this.age = age;
        this.surname = surname;
        this.name = name;
        this.Company = Company;
        this.Position = Position;
        this.Salary = Salary;
    }
    public void Show()
    {
        base.Show();
        Console.WriteLine("Компания: {0}\nДолжность: {1}\nОклад: {2}\n", Company, Position, Salary);
    }
}

class Programm
{
    static void Main()
    {
        Employee[] Workers = new Employee[5];
        Workers[0] = new Employee("Лукойл", "Уборщик", 50000, "Валера", "Иванович", 28, "М");
        Workers[1] = new Employee("Газпром", "Открыватель дверей", 103000, "Саша", "Дуанович", 33, "М");
        Workers[2] = new Employee("Технопоинт", "Директор", 1000, "Мария", "Лолинова", 14, "Ж");
        Workers[3] = new Employee("Лукойл", "Директор", 5000, "Илья", "Иванович", 35, "М");
        Workers[4] = new Employee("Лукойл", "Парковщик", 300, "Евангелина", "Ивановна", 80, "Ж");
        for (int i = 0; i < 5; i++)
            if (Workers[i].Company == "Лукойл" && Workers[i].sex == "Ж")
                Workers[i].Show();
        for (int i = 0; i < 5; i++)
            if (Workers[i].Company == "Лукойл" && Workers[i].sex == "М")
                Workers[i].Show();
    }
}