using System;

class Programm
{
    static void Main()
    {
        for (int i = 100; i <= 150; i += 2)
            Console.WriteLine("sqrt({0}) = {1}", i, System.Math.Sqrt(i));
    }
}