using System;
using System.Collections.Generic;

class oldB
{
    protected string name;
    protected string phone;

    public string Name
    {
        get { return name; }
    }

    public string Phone
    {
        get { return phone; }
    }

    public oldB(string name, string phone)
    {
        this.name = name;
        this.phone = phone;
    }

    public virtual void info()
    {
        Console.WriteLine("Кличка животного: {0}\nНомер телефона владельца: {1}", name, phone);
    }
}

class newB : oldB
{
    private string phonereg;

    public string Phonereg{
        get { return phonereg; }
    }

    public newB(string name, string phone, string phonereg):base(name, phone)
    {
        this.name = name;
        this.phone = phone;
        this.phonereg = phonereg;
    }

    public override void info()
    {
        base.info();
        Console.WriteLine("Номер телефона регистрации: {0}", phonereg);
    }
}

class Programm
{
    static void Main()
    {
        int n = 3;
        oldB[] a = new oldB[n];
        a[0] = new oldB("Мурка", "1240124");
        a[1] = new oldB("Жора", "1584362");
        a[2] = new newB("Вася", "12416161", "12516");
        foreach (oldB k in a)
        {
            if (k is newB)
            {
                k.info();
                Console.WriteLine("Телефон: {0}", ((newB)k).Phonereg);
                Console.WriteLine();
            }
        }
    }
}