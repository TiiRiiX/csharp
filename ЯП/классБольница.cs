using System;

class Building
{
    public int countFloor;
    public double height;
    public string name;
    public DateTime dataBuilded;
    public bool isPublic;

    public double HeightForOneFloor()
    {
        return height / countFloor;
    }

    public void About()
    {
        string str;
        str = "Здание называется " + name;
        str += "\nВысота = " + height;
        str += "\nКоличество этажей: " + countFloor;
        str += "\nПостроено " + dataBuilded;
        str += "\nВысота одного этажа = " + HeightForOneFloor();
        if (isPublic) str += "\nЭто публичное здание";
        else str += "\nЭто частное здание";
        Console.WriteLine(str);
    }
}


class Programm
{
    static void Main()
    {
        Building firstBuild = new Building();
        firstBuild.name = "Больница";
        firstBuild.countFloor = 4;
        firstBuild.height = 15;
        firstBuild.dataBuilded = new DateTime(2011, 5, 1, 8, 30, 52);
        firstBuild.isPublic = true;
        firstBuild.About();
        Console.ReadKey();
    }
}