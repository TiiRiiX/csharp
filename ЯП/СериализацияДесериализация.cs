using System;
using System.IO;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

[Serializable]
class Persona
{
    string name;
    double num;

    public string Name
    {
        get { return name; }
    }

    public Persona(string name, double num)
    {
        this.name = name;
        this.num = num;
    }
}

class Programm
{
    static void Main()
    {
        Persona man = new Persona("Lolka", 123);
        int[] lalal = { 1, 2, 3, 4, 5 };
        BinaryFormatter Format = new BinaryFormatter();
        FileStream fs = new FileStream("input.txt", FileMode.Open, FileAccess.Write);
        Format.Serialize(fs, man);
        Format.Serialize(fs, 1234);
        Format.Serialize(fs, lalal);
        fs.Close();
/*
        FileStream fout = new FileStream("input.txt", FileMode.Open, FileAccess.Read);
        Persona temp = (Persona)Format.Deserialize(fout);
        Console.WriteLine(temp.Name);
        */
    }
}