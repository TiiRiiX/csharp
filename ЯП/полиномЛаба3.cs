using System;

class Polynom
{
    public int maxFactor;
    public double[] factor;


    public void fillPolynom(int _maxFactor)
    {
        maxFactor = _maxFactor;
        factor = new double[maxFactor];
        for(int i = 0; i < maxFactor; i++)
        {
            Console.Write("Введите коофициент при степени " + i + " : ");
            factor[i] = double.Parse(Console.ReadLine());
        }
    }

    public void printPolynom()
    {
        string str = Convert.ToString(factor[0]);
        for (int i = 1; i < factor.Length; i++)
            str = factor[i] + "x^" + i + " + " + str;
        Console.WriteLine(str);
    }

    public double solveFor(double x)
    {
        double solve = 0;
        double powerX = 1;
        for (int i = 0; i < factor.Length; i++)
        {
            solve += factor[i] * (powerX);
            powerX *= x;
        }
        return solve;
    }

    public Polynom Plus(Polynom xNew)
    {
        Polynom newPolym = new Polynom();
        if (xNew.factor.Length > factor.Length) {
            newPolym.factor = new double[xNew.factor.Length];
            for (int i = 0; i < factor.Length; i++)
                newPolym.factor[i] = xNew.factor[i] + factor[i];
            for (int i = factor.Length; i < xNew.factor.Length; i++)
                newPolym.factor[i] = xNew.factor[i];
        }
        else { 
            newPolym.factor = new double[factor.Length];
            for (int i = 0; i < xNew.factor.Length; i++)
                newPolym.factor[i] = xNew.factor[i] + factor[i];
            for (int i = xNew.factor.Length; i < factor.Length; i++)
                newPolym.factor[i] = factor[i];
        }
        return newPolym; 
    }

    public Polynom Minus(Polynom xNew)
    {
        Polynom newPolym = new Polynom();
        if (xNew.factor.Length > factor.Length)
        {
            newPolym.factor = new double[xNew.factor.Length];
            for (int i = 0; i < factor.Length; i++)
                newPolym.factor[i] = factor[i] - xNew.factor[i];
            for (int i = factor.Length; i < xNew.factor.Length; i++)
                newPolym.factor[i] = xNew.factor[i];
        }
        else
        {
            newPolym.factor = new double[factor.Length];
            for (int i = 0; i < xNew.factor.Length; i++)
                newPolym.factor[i] = factor[i] - xNew.factor[i];
            for (int i = xNew.factor.Length; i < factor.Length; i++)
                newPolym.factor[i] = factor[i];
        }
        return newPolym;
    }
}


class Programm
{
    static void Main()
    {
        Console.Write("Введите степень многочлена: ");
        Polynom firstPolym = new Polynom();
        firstPolym.fillPolynom(int.Parse(Console.ReadLine()));
        firstPolym.printPolynom();
        Polynom secondPolynom = new Polynom();
        secondPolynom.fillPolynom(int.Parse(Console.ReadLine()));
        secondPolynom.printPolynom();
        Console.WriteLine("f(" + 5 + ")" + " = " + firstPolym.solveFor(5));
        Console.WriteLine("f(" + 5 + ")" + " = " + secondPolynom.solveFor(5));
        Polynom plusPolym = firstPolym.Plus(secondPolynom);
        plusPolym.printPolynom();
        Polynom minusPolym = firstPolym.Minus(secondPolynom);
        minusPolym.printPolynom();
        Console.ReadKey();
    }
}