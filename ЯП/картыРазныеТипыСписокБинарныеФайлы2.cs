using System;
using System.IO;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

[Serializable]
class oldCard //карты старого типа
{
    protected string fio; //поле фамилия имя отчество
    protected int b_day; //дата рождения
    protected int b_month;
    protected int b_year;

    public string Fio //свойство даты рождения
    {
        get { return fio; }
    }

    public oldCard(string fio, string birthday) //конструктор 
    {
        this.fio = fio;
        this.b_day = int.Parse(birthday.Split('.')[0]);
        this.b_month = int.Parse(birthday.Split('.')[1]);
        this.b_year = int.Parse(birthday.Split('.')[2]);
    }

    public virtual void info() //вывод информации о клиенте
    {
        Console.WriteLine("Клиент: {0}\nДата рождения: {1}.{2}.{3}", fio,b_day, b_month, b_year);
    }
}

[Serializable]
class newCard : oldCard //новые карты
{
    private string phone; //номер телефона

    public string Phone //свойство номер телефона
    {
        get { return phone; }
    }

    public newCard(string fio, string birthday, string phone) : base(fio, birthday) //конструктор ссылающийся на базовый класс
    {
        this.fio = fio;
        this.b_day = int.Parse(birthday.Split('.')[0]);
        this.b_month = int.Parse(birthday.Split('.')[1]);
        this.b_year = int.Parse(birthday.Split('.')[2]);
        this.phone = phone;
    }

    public override void info() //вывод информации
    {
        base.info(); //обращение к базовому классу
        Console.WriteLine("Номер телефона: {0}", phone);
    }
}

class Program
{
    static void Main(string[] args)
    {
        List<oldCard> shoppers = new List<oldCard>(); //коллекция карт
        try
        {
            StreamReader f = new StreamReader("C:/txts/input.txt"); //открываем поток на чтение
            while (!f.EndOfStream) //пока не достигнут конец файла
            {
                string[] temp = f.ReadLine().Split(' '); //читаем строку из файла и создаем из нее массив методом split
                if (temp.Length == 5) //если пять элеметнов, то это запись нового типа
                    shoppers.Add(new newCard(temp[0] + " " + temp[1] + " " + temp[2], temp[3], temp[4]));
                else //иначе старый тип
                    shoppers.Add(new oldCard(temp[0] + " " + temp[1] + " " + temp[2], temp[3]));
            }
            f.Close();//закрываем файл
        }
        catch (Exception e) //перехватываем ошибку
        {
            Console.WriteLine(e.Message); //выводим сообщение об ошибке
            Console.WriteLine("Входной файл не найден");
        }
        Console.WriteLine("Все достунпые записи: ");
        foreach (oldCard card in shoppers) //перебор всех элементов в списке с картами
        {
            card.info(); //выводим информацию о картах
            Console.WriteLine();
        }
        BinaryFormatter Format = new BinaryFormatter();//создание объекта сериализации
        FileStream fs = new FileStream("C:/txts/out.bin", FileMode.Open, FileAccess.Write);//открытие файла на запись
        Format.Serialize(fs, shoppers);//запись списка в бинарный файл
    }
}

/* Содержимое файла input.txt
Баранова Регина Викторовна 24.05.1982 89036833688
Яковлев Галактион Федорович 5.09.1982 89035591467
Соколова Владилена Филипповна 5.12.1984 89241442978
Куколевская Эльвира Владиславовна 13.08.1967
Трофимов Сергей Денисович 21.08.1989
Абрамова Анастасия Леонидовна 7.04.1971 89073568319
Фомова Кира Степановна 1.06.1977
*/
