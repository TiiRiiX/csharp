using System;

class Programm
{
    static void Main()
    {
        double i;
        Console.WriteLine("Число\tКвадрат\tКуб");
        for (i = 1; i <= 5; i+=0.5)
        {
            Console.WriteLine("{0}\t{1}\t{2}", i, i * i, i * i * i);
        }
        Console.WriteLine("Нажмите любую кнопку!");
        Console.ReadKey();
    }
}