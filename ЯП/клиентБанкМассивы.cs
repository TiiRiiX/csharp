using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Client
    {
        string surname;
        int passport;
        double[] money;
        int[] numbers;
        int count;

        public Client(string _surname, int _passport, int _count)
        {
            surname = _surname;
            passport = _passport;
            count = _count;
            money = new double[count];
            numbers = new int[count];
            for(int i = 0; i < count; i++)
            {
                numbers[i] = int.Parse(Console.ReadLine());
                money[i] = double.Parse(Console.ReadLine());
            }
        }

        public int Max()
        {
            int maxNum = 0;
            for (int i = 0; i < count; i++)
                if (money[i] > money[maxNum])
                    maxNum = i;
            return maxNum;
        }

        public double Sum()
        {
            double sum = 0;
            for (int i = 0; i < count; i++)
                sum += money[i];
            return sum;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Client[] Clients = new Client[2];
            Clients[0] = new Client("Иванов", 124151, 2);
            Clients[1] = new Client("Смирнов", 1000, 1);
            Console.ReadKey();
            
        }
    }
}
