using System;
using System.Collections.Generic;
using System.IO;

class Student//класс студента
{
    protected string name; //поле имя
    protected string faculty; //поле факультет
    protected int year;//поле год поступления
    private int rating;//поле рейтинга

    public int Rating //открытое свойство рейтинга
    {
        get { return rating; }
    }

    public string Faculty //факультет
    {
        get { return faculty; }
    }

    public string Name //имя
    {
        get { return name; }
    }

    public Student(string name, string faculty, int year, int rating) //конструктор для студента
    {
        this.name = name;
        this.faculty = faculty;
        this.year = year;
        this.rating = rating;
    }

    public Student(string name, string faculty, int year) //конструктор для аспиранта
    {
        this.name = name;
        this.faculty = faculty;
        this.year = year;
    }

    public void info() //метод для вывода информации
    {
        Console.WriteLine("Имя: {0}\nФакультет: {1}\nГод поступления: {2}\nРейтинг: {3}\n", name, faculty, year, rating);
    }
}

class Aspirant : Student //класс аспиранта
{
    private string head; //руководитель
    private string key; //код специальности

    public string Key //свойство кода специальности
    {
        get { return key; }
    }

    public int Year //свойство года поступления
    {
        get { return year; }
    }

    public Aspirant(string head, string key, string name, string faculty, int year) : base(name, faculty, year) //конструктор аспирант с обращением к базовому
    {
        this.name = name;
        this.faculty = faculty;
        this.year = year;
        this.head = head;
        this.key = key;
    }

    new public void info() //вывод информации об аспиранте
    {
        Console.WriteLine("Имя: {0}\nФакультет: {1}\nГод поступления: {2}\nРуководитель: {3}\nКод специальности: {4}\n", name, faculty, year, head, key);
    }
}

class Programm
{
    static void Main()
    {
        List<Student> Students = new List<Student>(); //список студентов
        List<Aspirant> Aspirants = new List<Aspirant>(); //список аспирантов
        try
        {
            StreamReader f = new StreamReader("input.txt"); //открываем поток на чтение
            while (!f.EndOfStream) //пока не достигнут конец файла
            {
                string[] temp = f.ReadLine().Split(' '); //читаем строку из файла и создаем из нее массив методом split
                if (temp.Length == 4) //если пять элеметнов, то это запись нового типа
                    Students.Add(new Student(temp[0], temp[1], int.Parse(temp[2]), int.Parse(temp[3])));
                else //иначе старый тип
                    Aspirants.Add(new Aspirant(temp[0], temp[1], temp[2], temp[3], int.Parse(temp[4])));
            }
            f.Close();//закрываем файл
        }
        catch (Exception e) //перехватываем ошибку
        {
            Console.WriteLine(e.Message); //выводим сообщение об ошибке
            Console.WriteLine("Входной файл не найден");
        }
        int max1 = 0, max2 = 0, max3 = 0; //три максимальных балла
        Student max1_s = null, max2_s = null, max3_s = null; //для запоминания записей с максимальным рейтингом
        foreach (Student a in Students)
        {
            if (a.Faculty == "экономический") //если с экономического факультета
                if (a.Rating > max1) //находим максимумы
                {
                    max3 = max2; max2 = max1; max1 = a.Rating;
                    max3_s = max2_s; max2_s = max1_s; max1_s = a;
                }
                else if (a.Rating > max2)
                {
                    max3 = max2; max2 = a.Rating;
                    max3_s = max2_s; max2_s = a;
                }
                else if (a.Rating > max3)
                {
                    max3 = a.Rating;
                    max3_s = a;
                }
        }
        Console.WriteLine("Аспиранты, что заканчивают завершают в этом (2017) году с кодом 051311: \n");
        foreach (Aspirant a in Aspirants) //перебор всех аспирантов
            if (a.Year + 3 == 2017 && a.Key == "051311") //ищем окончание в 2017 году и нужный код специальности
                a.info(); //выводим информацию
        Console.WriteLine("Трое студента с экономического факультета с максимальным рейтингом: \n");
        max1_s.info();//выводим информацию о студентах с максимальным рейтингом
        max2_s.info();
        max3_s.info();
        Console.WriteLine("Информация о всех студентах: \n");
        foreach (Student a in Students) //перебор всех студентов
            a.info(); //вывод информации
        Console.WriteLine("Информация о всех аспирантах: \n");
        foreach (Aspirant a in Aspirants) //перебор всех аспирантов
            a.info();//вывод информации
    }
}
/* Содержимое файла input.txt
Анатолий экономический 2011 2
Иваненко 051311 Наташа географический 2013
Вася экономический 2014 9
Петя экономический 2015 8
Коваленко 051311 Илья географический 2014
Юра экономический 2013 5
Олеся юридический 2016 4
Вова юридический 2015 7
Параманенко 051310 Марина языковой 2014
*/