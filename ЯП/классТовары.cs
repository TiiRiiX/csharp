using System;

class goods
{
    public string ProductName;
    public DateTime ExpireDate;
    public double Price;
}

class Example
{

    static void Main()
    {
        goods Bread = new goods();
        Bread.ProductName = "Хлеб Изобилие";
        Bread.ExpireDate = new DateTime(2011, 5, 1, 8, 30, 52);
        Bread.Price = 43.54;

        goods Milk = new goods();
        Milk.ProductName = "Молоко";
        Milk.ExpireDate = new DateTime(2011, 5, 5, 6, 30, 52);
        Milk.Price = 35.6;

        goods Chocolote = new goods();
        Console.Write("Введие название нового продукта :");
        Chocolote.ProductName = Console.ReadLine();
        Console.Write("Введите дату срока годности (гггг, мм, дд, час, мин, сек): ");
        Chocolote.ExpireDate = new DateTime(int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()));
        Console.Write("Введите цену ногово продукта: ");
        Chocolote.Price = double.Parse(Console.ReadLine());

        string str;
        str = "Товар: " + Bread.ProductName + "\nСрок годности: " + Bread.ExpireDate + "\nЦена: " + Bread.Price + " рубля";
        str += "\nТовар: " + Milk.ProductName + "\nСрок годности: " + Milk.ExpireDate + "\nЦена: " + Milk.Price + " рубля";
        str += "\nТовар: " + Chocolote.ProductName + "\nСрок годности: " + Chocolote.ExpireDate + "\nЦена: " + Chocolote.Price + " рубля";


        Console.WriteLine(str);
        Console.WriteLine("Нажмите любую кнопку!");
        Console.ReadKey();
    }
}