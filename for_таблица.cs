using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            double y, h;
            Console.WriteLine("y | H");
            Console.WriteLine("_____");
            for (y = 0.1; y <= 0.2; y = y + 0.01)
            {
                h = 2 * y * y + 4 * y - 0.3;
                Console.WriteLine("{0} | {1}", y, h);
            }
            Console.ReadKey();
        }
    }
}
