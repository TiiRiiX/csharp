﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;


namespace Lab6 {
    public partial class Form1 : Form {
        List<Thread> thread = new List<Thread>();
        Random r = new Random();
        static Mutex mut = new Mutex();
        int num = 0;
        bool random = true;
        Thread main;

        public Form1() {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e) {
            button3.BackColor = colorDialog1.Color;
            main = Thread.CurrentThread;
        }

        private void button3_Click(object sender, EventArgs e) {
            if (colorDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            button3.BackColor = colorDialog1.Color;
        }

        public void drawning() {
            Color color;
            if (random) {
                color = Color.FromArgb(r.Next(0, 255), r.Next(0, 255), r.Next(0, 255));
            } else {
                color = colorDialog1.Color;
            }
            Graphics g = draw.CreateGraphics();
            SolidBrush brush = new SolidBrush(color);
            int x, y;
            while (true) {
                mut.WaitOne();
                x = r.Next(draw.Width);
                y = r.Next(draw.Height);
                float rad = (float)numericUpDown1.Value;
                g.FillEllipse(brush, x, y, rad, rad);
                mut.ReleaseMutex();
                Thread.Sleep((int)numericUpDown2.Value);
            }
        }

        private void dr() {
            while (true) {
                int a = 1;
                for (int i = 1; i < 1000000; i++)
                    a *= i;

                drawning();
            }
        }
        Thread thr;
        private void button2_Click(object sender, EventArgs e) {
            if (listBox1.Items.Count > 0 && listBox1.SelectedItem.ToString() != "") {
                mut.WaitOne();
                thr = thread.Find(x => x.Name == listBox1.SelectedItem.ToString());
                thread.Remove(thr);
                thr.Abort();
                listBox1.Items.Remove(listBox1.SelectedItem.ToString());
                if (listBox1.Items.Count > 0)
                    listBox1.SelectedIndex = listBox1.Items.Count - 1;
                num--;
                mut.ReleaseMutex();
            }
        }

        private void button4_Click(object sender, EventArgs e) {
            num++;
            random = true;
            Thread myThread = new Thread(dr);
            thread.Add(myThread);
            myThread.Name = "Поток " + num.ToString();
            listBox1.Items.Add(myThread.Name);
            listBox1.SelectedItem = myThread.Name;
            if ((string)comboBox2.SelectedItem == "Высокий") myThread.Priority = ThreadPriority.Highest;
            if ((string)comboBox2.SelectedItem == "Выше среднего") myThread.Priority = ThreadPriority.AboveNormal;
            if ((string)comboBox2.SelectedItem == "Средний") myThread.Priority = ThreadPriority.Normal;
            if ((string)comboBox2.SelectedItem == "Ниже среднего") myThread.Priority = ThreadPriority.BelowNormal;
            if ((string)comboBox2.SelectedItem == "Низкий") myThread.Priority = ThreadPriority.Lowest;
            myThread.IsBackground = true;
            myThread.Start();
        }

        private void button5_Click(object sender, EventArgs e) {
            if (listBox1.Items.Count > 0 && listBox1.SelectedItem.ToString() != "") {
                thr = thread.Find(x => x.Name == listBox1.SelectedItem.ToString());
                thr.Suspend();
            }
        }

        private void button1_Click(object sender, EventArgs e) {
            num++;
            random = false;
            Thread myThread = new Thread(dr);
            thread.Add(myThread);
            myThread.Name = "Поток " + num.ToString();
            listBox1.Items.Add(myThread.Name);
            listBox1.SelectedItem = myThread.Name;
            if ((string)comboBox2.SelectedItem == "Высокий") myThread.Priority = ThreadPriority.Highest;
            if ((string)comboBox2.SelectedItem == "Выше среднего") myThread.Priority = ThreadPriority.AboveNormal;
            if ((string)comboBox2.SelectedItem == "Средний") myThread.Priority = ThreadPriority.Normal;
            if ((string)comboBox2.SelectedItem == "Ниже среднего") myThread.Priority = ThreadPriority.BelowNormal;
            if ((string)comboBox2.SelectedItem == "Низкий") myThread.Priority = ThreadPriority.Lowest;
            myThread.IsBackground = true;
            myThread.Start();
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e) {
            if ((string)comboBox3.SelectedItem == "Высокий") main.Priority = ThreadPriority.Highest;
            if ((string)comboBox3.SelectedItem == "Выше среднего") main.Priority = ThreadPriority.AboveNormal;
            if ((string)comboBox3.SelectedItem == "Средний") main.Priority = ThreadPriority.Normal;
            if ((string)comboBox3.SelectedItem == "Ниже среднего") main.Priority = ThreadPriority.BelowNormal;
            if ((string)comboBox3.SelectedItem == "Низкий") main.Priority = ThreadPriority.Lowest;
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e) {
            if (listBox1.SelectedItem.ToString() != "") {
                thr = thread.Find(x => x.Name == listBox1.SelectedItem.ToString());
                if ((string)comboBox2.SelectedItem == "Высокий") thr.Priority = ThreadPriority.Highest;
                if ((string)comboBox2.SelectedItem == "Выше среднего") thr.Priority = ThreadPriority.AboveNormal;
                if ((string)comboBox2.SelectedItem == "Средний") thr.Priority = ThreadPriority.Normal;
                if ((string)comboBox2.SelectedItem == "Ниже среднего") thr.Priority = ThreadPriority.BelowNormal;
                if ((string)comboBox2.SelectedItem == "Низкий") thr.Priority = ThreadPriority.Lowest;
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e) {

        }

        private void button6_Click(object sender, EventArgs e) {
            if (listBox1.Items.Count > 0 && listBox1.SelectedItem.ToString() != "") {
                thr = thread.Find(x => x.Name == listBox1.SelectedItem.ToString());
                thr.Resume();
            }
        }
    }
}
