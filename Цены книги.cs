//Сусанов Павел МММ
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            double price, money;
            Console.Write("Введите стоимость книги: ");
            price = Convert.ToDouble(Console.ReadLine());
            Console.Write("Введите количество денег: ");
            money = Convert.ToDouble(Console.ReadLine());
            if (price > money) Console.WriteLine("Вам не хватает {0} рублей", price - money);
            if (price == money) Console.WriteLine("Спасибо!");
            if (price < money) Console.WriteLine("Спасибо, возьмите сдачу {0}", money - price);
            Console.ReadKey();
        }
    }
}
/*
 * price = 200,money = 120, Не хватает 80
 * price = 20, money = 20, Спасибо
 * price = 10, money = 340, Сдача 330
 */
